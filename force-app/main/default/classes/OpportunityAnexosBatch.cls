/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Class reponsible for updtade Anexos fields on Opportunity records
*
*   Test class : OpportunityAnexosBatch_test
*   Called from : OpportunityAnexosScheduler
*
* NAME: OpportunityAnexosBatch
* AUTHOR: João Vitor Ramos                                     DATE: 03/02/2021
*******************************************************************************/
global class OpportunityAnexosBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(
            'SELECT Id, Anexos__c FROM Opportunity'
        );
    }

    global void execute(Database.BatchableContext bc, List<Opportunity> records){
        // process each batch of 200 of records
        //creating list aggregate value lists of tasks, events and documents
        List<Opportunity> oppUpdList = new List<Opportunity>(); 
        List<Id> opportunityId = new List<Id>();
        for(Opportunity opp : records){
            opportunityId.add(opp.id);
        }

        List<AggregateResult> attachmentNoteList = [SELECT LinkedEntityId, Count(ContentDocumentId) Anexos 
                                                        FROM ContentDocumentLink GROUP BY LinkedEntityId HAVING LinkedEntityId IN :opportunityId];
        
        //passing through the Opportunity records, macthing the Opportunity Ids with the Activity and Document Ids
        //to update the Opportunity field values with the new arrangement values
        if(!attachmentNoteList.isEmpty()){
            for(Opportunity opp : Records){
                for(AggregateResult cdl : attachmentNoteList){
                    if(opp.id == (Id)cdl.get('LinkedEntityId')){
                        opp.Anexos__c = (Integer)cdl.get('Anexos');
                        oppUpdList.add(opp);
                        break;
                    }
                }
            }
        }
        
        //updating the Opportunity fields
        Database.SaveResult[] results = Database.update(oppUpdList, false);

        for(Database.SaveResult sr : results){
            if(sr.isSuccess()){
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated Opportunity. Opportunity ID: ' + sr.getId());
            } 
            else{
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Opportunity fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        //Nothing to do after the execution for awhile
    }    
}
