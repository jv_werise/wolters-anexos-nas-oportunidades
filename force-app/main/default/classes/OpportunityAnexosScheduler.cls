/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Class reponsible for call the batch class to updtade some fields on Opportunity records
*
*   Test class : OpportunityAnexosBatch_test
*   Call: OpportunityAnexosBatch
*
* NAME: OpportunityAnexosScheduler
* AUTHOR: João Vitor Ramos                                     DATE: 03/02/2021
*******************************************************************************/

global class OpportunityAnexosScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        OpportunityAnexosBatch oab = new OpportunityAnexosBatch();
        Id batchId = Database.executeBatch(oab);
        System.debug(batchId);
    }
}
