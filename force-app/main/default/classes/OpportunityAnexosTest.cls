/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Class reponsible for testing the triggers and handler classes 
*   that update some fields on Opportunity records
*
*   Test: OpportunityAnexosHandler && ContentDocumentLinkTrigger && ContentDocumentTrigger
*
* NAME: OpportunityAnexosTest
* AUTHOR: João Vitor Ramos                                      DATE: 10/02/2021
*******************************************************************************/
@isTest
public class OpportunityAnexosTest {
    @testSetup
    private static void setup(){
        //creating Account, opportunity and its documents
        Account acc = new Account();
        acc.RecordTypeId = [Select id from RecordType where name = 'Partner'][0].Id;
        acc.Name = 'Account Test';
        acc.Official_Name__c = 'Official Account Test';
        acc.Status__c = 'Active';
        acc.Internal_Code__c = '9999999';
        acc.form_of_payment__c = getform_of_payment();
        acc.State_Registration__c = '999999';
        insert acc;

        Opportunity opp = new Opportunity();
        opp.RecordTypeId = [Select id from RecordType where name = 'Product'][0].Id;
        opp.Name = 'Opp Test';
        opp.AccountId = acc.Id;
        opp.Type = getType();
        opp.StageName = getStageName();
        opp.CloseDate = Date.today().addMonths(1);
        Insert opp;

        ContentNote cnt = new ContentNote();
        cnt.Content = Blob.valueof('Testing the note creation');
        cnt.Title = 'OppNote';
        insert cnt;
    }

    @isTest
    private static void testInsertDoc(){
        Opportunity opp = [SELECT id FROM Opportunity][0];
        ContentNote cnt = [SELECT id FROM ContentNote][0];

        Test.startTest();
        ContentDocumentLink cd = new ContentDocumentLink();
        cd.LinkedEntityId = opp.Id;
        cd.ContentDocumentId = cnt.id;
        insert cd;
        Test.stopTest();

        System.assertEquals(1, [select count() from Opportunity where Anexos__c = 1]);
    }

    @isTest
    private static void testDeleteDoc(){
        Opportunity opp = [SELECT id FROM Opportunity][0];
        ContentNote cnt = [SELECT id FROM ContentNote][0];

        ContentDocumentLink cd = new ContentDocumentLink();
        cd.LinkedEntityId = opp.Id;
        cd.ContentDocumentId = cnt.id;
        insert cd;

        Test.startTest();
        delete cd;
        delete cnt;
        Test.stopTest();

        System.assertEquals(1, [select count() from Opportunity where Anexos__c = 0]);
    }

    //methods to take the first value of picklists dynamically
    public static String getform_of_payment(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.form_of_payment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
    public static String getType(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
    public static String getStageName(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList[0];
    }
}
