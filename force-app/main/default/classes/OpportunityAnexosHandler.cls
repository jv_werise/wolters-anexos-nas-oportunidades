/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Class reponsible for updtade Anexos fields on Opportunity records
*
*   Test class : OpportunityAnexosTest
*   Called from : ContentDocumentLinkTrigger && ContentDocumentTrigger
*
* NAME: OpportunityAnexosHandler
* AUTHOR: João Vitor Ramos                                     DATE: 10/02/2021
*******************************************************************************/
public with sharing class OpportunityAnexosHandler {
    public static void insertDocLink(list<ContentDocumentLink> records){
        set<Id> entityIdSet = new set<Id>();
        set<Id> oppIdSet = new set<Id>();
        list<Opportunity> oppList = new list<Opportunity>();
        list<Opportunity> oppUpdList = new list<Opportunity>();
        List<AggregateResult> attachmentNoteList = new List<AggregateResult>();

        for(ContentDocumentLink cdl : records){
            EntityIdSet.add(cdl.LinkedEntityId);
        }

        if(!EntityIdSet.isEmpty()){
            for(Opportunity opp : [select Id, Anexos__c from Opportunity where Id IN :EntityIdSet]){
                oppIdSet.add(opp.Id);
                oppList.add(opp);
            }
        }

        System.debug(oppIdSet);
        System.debug(oppList);

        if(!oppIdSet.isEmpty()){
             attachmentNoteList = [SELECT LinkedEntityId, Count(ContentDocumentId) Anexos 
                                    FROM ContentDocumentLink GROUP BY LinkedEntityId HAVING LinkedEntityId IN :oppIdSet];
        }

        if(!attachmentNoteList.isEmpty()){
            for(Opportunity opp : oppList){
                for(AggregateResult cdl : attachmentNoteList){
                    if(opp.id == (Id)cdl.get('LinkedEntityId')){
                        opp.Anexos__c = (Integer)cdl.get('Anexos');
                        oppUpdList.add(opp);
                        break;
                    }
                }
            }
        }

        //updating the Opportunity fields
        Database.SaveResult[] results = Database.update(oppUpdList, false);

        for(Database.SaveResult sr : results){
            if(sr.isSuccess()){
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated Opportunity. Opportunity ID: ' + sr.getId());
            } 
            else{
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Opportunity fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    public static void deleteDocLink(list<ContentDocumentLink> records){
        set<Id> entityIdSet = new set<Id>();
        list<Opportunity> oppUpdList = new list<Opportunity>();

        for(ContentDocumentLink cdl : records){
            entityIdSet.add(cdl.LinkedEntityId);
        }

        if(!EntityIdSet.isEmpty()){
            for(Opportunity opp : [select Id, Anexos__c from Opportunity where Id In :entityIdSet]){
                opp.Anexos__c = opp.Anexos__c - 1;
                oppUpdList.add(opp);
            }
        }

        //updating the Opportunity fields
        Database.SaveResult[] results = Database.update(oppUpdList, false);

        for(Database.SaveResult sr : results){
            if(sr.isSuccess()){
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated Opportunity. Opportunity ID: ' + sr.getId());
            } 
            else{
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Opportunity fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    public static void deleteDoc(list<ContentDocument> records){
        set<Id> entityIdSet = new set<Id>();
        set<Id> DocumentIdSet = new set<Id>();
        list<Opportunity> oppUpdList = new list<Opportunity>();

        for(ContentDocument cd : records){
            DocumentIdSet.add(cd.Id);
        }

        for(ContentDocumentLink cdl : [select LinkedEntityId from ContentDocumentLink 
                                  where ContentDocumentId IN :DocumentIdSet]){
            entityIdSet.add(cdl.LinkedEntityId);
        }

        for(Opportunity opp : [select Id, Anexos__c from Opportunity where Id In :entityIdSet]){
            opp.Anexos__c = opp.Anexos__c - 1;
            oppUpdList.add(opp);
        }
        //updating the Opportunity fields
        Database.SaveResult[] results = Database.update(oppUpdList, false);

        for(Database.SaveResult sr : results){
            if(sr.isSuccess()){
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated Opportunity. Opportunity ID: ' + sr.getId());
            } 
            else{
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Opportunity fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}
