/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Trigger on ContentDocumentLink SObject for (Insert, Update, Delete or Undelete) 
*   && (After or Before)
*
* 	For Insert:
*   OpportunityAnexosHandler.insertDocLink(Trigger.new);
*
*   For Delete:
*	OpportunityAnexosHandler.deleteDocLink(Trigger.old);
*   
* NAME: ContentDocumentLinkTrigger
* AUTHOR: João Vitor Ramos                                DATE: 10/02/2021
*******************************************************************************/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, before update, before delete, after insert, after update, after undelete) {
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            System.debug('ContentDocumentLink trigger - After Insert');
            OpportunityAnexosHandler.insertDocLink(Trigger.new);
        }
    }
    if(Trigger.isDelete){
        if(Trigger.isBefore){
            System.debug('ContentDocumentLink trigger - Before Delete');
            OpportunityAnexosHandler.deleteDocLink(Trigger.old);
        }
    }
}