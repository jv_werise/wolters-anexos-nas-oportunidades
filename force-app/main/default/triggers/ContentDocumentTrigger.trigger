/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Trigger on ContentDocument SObject for (Insert, Update, Delete or Undelete) 
*   && (After or Before)
*
* 	For Delete:
*   OpportunityAnexosHandler.deleteDoc(Trigger.old);
*	
*   
* NAME: ContentDocumentTrigger
* AUTHOR: João Vitor Ramos                                DATE: 10/02/2021
*******************************************************************************/
trigger ContentDocumentTrigger on ContentDocument (before insert, before update, before delete, after insert, after update, after undelete) {
    if(Trigger.isDelete){
        if(Trigger.isBefore){
            System.debug('ContentDocument trigger - Before Delete');
            OpportunityAnexosHandler.deleteDoc(Trigger.old);
        }
    }

}